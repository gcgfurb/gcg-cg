/// \file main.cpp
/// \brief N4_C_SelecionaAlpha: demonstra o uso do campo alpha da cor para fazer selecao de objetos. O valor de 255 representa cor sem Id para objeto. 
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 03/05/11.
/// Obs.: variaveis globais foram usadas por questoes didaticas mas nao sao recomendas para aplicacoes reais.

#include "constantes.h"
#include "objGrafico.h"
#include "../../../vart/time.h"
#include "../../../vart/mathdef.h"

GLint gJanelaPrincipal = 0;
GLint janelaLargura  = 400, janelaAltura = 400;
GLfloat ortho2D_minX = -400.0f, ortho2D_maxX =  400.0f, ortho2D_minY = -400.0f, ortho2D_maxY =  400.0f;

enum EKeyCode
{
    RAS_ESCAPE_KEY			= 27,
    RAS_SPECIAL_KEY_OFFSET  = 256,
    RAS_1                   = 49,
    RAS_2                   = 50,
    RAS_3                   = 51,
    RAS_a                   =  97,          RAS_A                   = 65,
    RAS_b                   =  98,          RAS_B                   = 66,
    RAS_c                   =  99,          RAS_C                   = 67,
    RAS_m                   = 109,          RAS_M                   = 77,
    RAS_o                   = 111,          RAS_O                   = 79,
    RAS_r                   = 114,          RAS_R                   = 82,
    RAS_LEFT_ARROW_KEY		= GLUT_KEY_LEFT,
    RAS_UP_ARROW_KEY		= GLUT_KEY_UP,
    RAS_RIGHT_ARROW_KEY		= GLUT_KEY_RIGHT,
    RAS_DOWN_ARROW_KEY		= GLUT_KEY_DOWN,
    RAS_PAGE_UP_KEY			= GLUT_KEY_PAGE_UP,
    RAS_PAGE_DOWN_KEY		= GLUT_KEY_PAGE_DOWN,
    RAS_HOME_KEY			= GLUT_KEY_HOME,
    RAS_END_KEY				= GLUT_KEY_END,
};

void inicializa(void);
void criaCena(void);
void transRotacao(void);
void transEscalaAmplia(void);
void transEscalaReduz(void);
void redimensiona(int w, int h);
void desenha(void);
void desenhaSRU(void);
void teclaPressionadaEspecial(int tecla, GLint px, GLint py);
void teclaPressionada(unsigned char tecla, GLint x, GLint y);
void depuraMatriz(void);
void depuraListaObjGrafico(void);
void exibeBBox(void);
void limpaMemoria(void);
void animacao(void);

void desenhaTexto(int x, int y, std::string text);
GLubyte pegaPixelTela(GLint cursorX, GLint cursorY);
void mouseMovimento(GLint x, GLint y);
void mouseEvento(GLint botao, GLint estado, GLint x, GLint y);
void calculaFPS(void);

//...


std::list<objGrafico> objGraficoLista;
std::list<objGrafico>::iterator objGraficoListaIt, objGraficoListaItSelec;
unsigned char objGraficoId;

bool animacaoFlag = 0;

#pragma mark TRANSFORMACOES
VART::Transform matrizTranslacao;
double	transLx, transLy, transLz, transSx, transSy, transSz;
VART::Transform matrizTranslacaoInversa;    
VART::Transform matrizEscala;
VART::Transform matrizRotacao;
double angRotacao = 5.0;

float fpsTempoDecorrido;
double fpsDuracao;
int fpsFrames;
VART::Time fpsUltimoTick, fpsCurrenteTick;
double fps;

int mousePosX, mousePosY;
char mousePosText[40];

#pragma mark INICIALIZACOES
void inicializa(void) {
	glClearColor(1.0f,1.0f,1.0f,1.0);

    criaCena();

    transLx = transLy = transLz =  2.0f;
    transSx =  transSy = transSz = 2.0;
    
//    fpsUltimoTick.NOW();
}

void criaCena(void) {
    objGrafico obj1 = *new objGrafico();
    obj1.setId(1);
    obj1.setCorId(VART::Color::CYAN(),obj1.getId());
    obj1.setTipo(objGrafico::SOLID_CUBE);
    obj1.calculaBoundingBox();
    objGraficoLista.push_back(obj1);

    objGrafico obj2 = *new objGrafico();
    obj2.setId(2);
    obj2.setCorId(VART::Color::YELLOW(),obj2.getId());
    obj2.setTipo(objGrafico::POLYGON);
    obj2.setVertice(VART::Point4D(5.0,0.0,5.0,1.0));
    obj2.setVertice(VART::Point4D(10.0,0.0,5.0,1.0));
    obj2.setVertice(VART::Point4D(10.0,0.0,10.0,1.0));
    obj2.setVertice(VART::Point4D(5.0,0.0,10.0,1.0));
    obj2.calculaBoundingBox();
    objGraficoLista.push_back(obj2);

    objGraficoListaItSelec = objGraficoLista.end();
    objGraficoId = 255; // valor de 255 representa cor sem Id para objeto.
}

void transRotacao(void) {
    VART::Point4D pto = objGraficoListaItSelec->getBBox()->GetCenter();
    
    // rotacao em si
    matrizRotacao.MakeZRotation(RAS_DEG_TO_RAD * angRotacao);
    
    // tranlacao inversa, voltando a posicao original
    matrizTranslacaoInversa.MakeTranslation(pto);
    
    // tranlacao para origem
    pto = -pto;
    matrizTranslacao.MakeTranslation(pto);
    
    objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * (matrizTranslacaoInversa * (matrizRotacao * matrizTranslacao)));
}

void transEscalaAmplia(void) {
    VART::Point4D pto = objGraficoListaItSelec->getBBox()->GetCenter();

    // escala em si
    matrizEscala.MakeScale(transSx, transSy, transSz);

    // tranlacao inversa, voltando a posicao original
    matrizTranslacaoInversa.MakeTranslation(pto);
    
    // tranlacao para origem
    pto = -pto;
    matrizTranslacao.MakeTranslation(pto);
    
    objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * (matrizTranslacaoInversa * (matrizEscala * matrizTranslacao)));
}

void transEscalaReduz(void) {
    VART::Point4D pto = objGraficoListaItSelec->getBBox()->GetCenter();

    // escala em si
    matrizEscala.MakeScale(1/transSx, 1/transSy, 1/transSz);

    // tranlacao inversa, voltando a posicao original
    matrizTranslacaoInversa.MakeTranslation(pto);
    
    // tranlacao para origem
    pto = -pto;
    matrizTranslacao.MakeTranslation(pto);
    
    objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * (matrizTranslacaoInversa * (matrizEscala * matrizTranslacao)));
}

#pragma mark REDIMENSIONA e DESENHA
void redimensiona(int w, int h) {
    //	glViewport(0,0,(GLsizei)w,(GLsizei)h);    // nao usar por enquanto...    
	glutPostRedisplay();
}

void desenha(void) {
    fpsUltimoTick = VART::Time::NOW(); // calcula FPS

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60, 1, 0.1, 100);
    gluLookAt(20, 20, 20, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    desenhaSRU();
    
    for (objGraficoListaIt = objGraficoLista.begin(); objGraficoListaIt != objGraficoLista.end(); objGraficoListaIt++) {
        objGraficoListaIt->desenha();
    }
    
    calculaFPS();
    sprintf(mousePosText,"ObjId [%u] - FPS [%3.2f] - Mouse[%i,%i] - Animacao[%i]", objGraficoId,fps,mousePosX, mousePosY, animacaoFlag);
    desenhaTexto(10, 10, mousePosText);    
	glutSwapBuffers();
}

void desenhaSRU(void) {
	// eixo x
	glLineWidth(1.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin( GL_LINES );
        glVertex3f(   0.0f, 0.0f, 0.0f );
        glVertex3f(  10.0f, 0.0f, 0.0f );
	glEnd();
	// eixo y
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin( GL_LINES);
        glVertex3f(  0.0f,   0.0f, 0.0f);
        glVertex3f(  0.0f,  10.0f, 0.0f );
  	glEnd();
	// eixo z
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin( GL_LINES);
    glVertex3f(  0.0f,  0.0f,  0.0f);
    glVertex3f(  0.0f,  0.0f, 10.0f );
  	glEnd();
}

void desenhaTexto(int x, int y, std::string text) {      
	glColor3f(0.0f, 0.0f, 0.0f);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, janelaLargura, 0, janelaAltura);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glRasterPos2i(x, y);
    for (unsigned int i=0; i<=text.size(); i++) 
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, text[i]);
}  

GLubyte pegaPixelTela(GLint cursorX, GLint cursorY) {
	GLint viewport[4];
	GLubyte pixel[4];
    
	glGetIntegerv(GL_VIEWPORT,viewport);
	glReadPixels(cursorX,viewport[3]-cursorY,1,1,GL_RGBA,GL_UNSIGNED_BYTE,(void *)pixel);
    return pixel[3];
}

#pragma mark TECLADO e MOUSE
void teclaPressionadaEspecial(int tecla, GLint px, GLint py) {
	switch (tecla) {
        case RAS_RIGHT_ARROW_KEY:
            if (objGraficoListaItSelec != objGraficoLista.end()) {
                matrizTranslacao.MakeTranslation(VART::Point4D(transLx,0,0));
                objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * matrizTranslacao);
            }
            break;
        case RAS_LEFT_ARROW_KEY:
            if (objGraficoListaItSelec != objGraficoLista.end()) {
                matrizTranslacao.MakeTranslation(VART::Point4D(-transLx,0,0));
                objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * matrizTranslacao);
            }
            break;
        case RAS_UP_ARROW_KEY:
            if (objGraficoListaItSelec != objGraficoLista.end()) {
                matrizTranslacao.MakeTranslation(VART::Point4D(0,transLy,0));
                objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * matrizTranslacao);
            }
            break;
        case RAS_DOWN_ARROW_KEY:
            if (objGraficoListaItSelec != objGraficoLista.end()) {
                matrizTranslacao.MakeTranslation(VART::Point4D(0,-transLy,0));
                objGraficoListaItSelec->setMatriz(objGraficoListaItSelec->getMatriz() * matrizTranslacao);
            }
            break;
        case RAS_PAGE_UP_KEY:
            break;
        case RAS_PAGE_DOWN_KEY:
            break;
        default:
            std::cout << " Invalid keyword! " << std::endl;
    }
	glutPostRedisplay();     
}

void teclaPressionada(unsigned char tecla, GLint x, GLint y) {
	switch (tecla) {
        case RAS_ESCAPE_KEY:
            exit(EXIT_SUCCESS);
            break;
		case RAS_r: case RAS_R:
            if (objGraficoListaItSelec != objGraficoLista.end()) {
                objGraficoListaItSelec->matrizIdentidade();
                objGraficoListaItSelec->calculaBoundingBox();
            }
            break;
        case RAS_a: case RAS_A:
            animacaoFlag = !animacaoFlag;
            break;
        case RAS_b: case RAS_B:
            exibeBBox();
            break;
		case RAS_c: case RAS_C:
            limpaMemoria();
            break;
		case RAS_m: case RAS_M:
            depuraMatriz();
            break;			
		case RAS_o: case RAS_O:
            depuraListaObjGrafico();
            break;
        case RAS_1:
            if (objGraficoListaItSelec != objGraficoLista.end())
                transRotacao();
            break;
        case RAS_2:
            if (objGraficoListaItSelec != objGraficoLista.end())
                transEscalaReduz();
            break;
        case RAS_3:
            if (objGraficoListaItSelec != objGraficoLista.end())
                transEscalaAmplia();
            break;
        default:
            std::cout << " Invalid keyword! " << std::endl;
    }
	glutPostRedisplay();    
}

void mouseMovimento(GLint x, GLint y) {
    mousePosX = x; mousePosY = y;    
	glutPostRedisplay();    
}

void mouseEvento(GLint botao, GLint estado, GLint x, GLint y) {
    if (estado == GLUT_DOWN) {
        objGraficoId = pegaPixelTela(x,y);

        objGraficoListaItSelec = objGraficoLista.end();
        for (objGraficoListaIt = objGraficoLista.begin(); objGraficoListaIt != objGraficoLista.end(); objGraficoListaIt++) {
            if (objGraficoListaIt->getId() == objGraficoId ) {
                objGraficoListaItSelec = objGraficoListaIt;
                break;
            }
        }

    }
    glutPostRedisplay();
}

#pragma mark DEPURACOES
void depuraMatriz(void) {
    if (objGraficoListaItSelec != objGraficoLista.end()) {
        std::cout << "--- depuraMatriz [Objeto: " << objGraficoListaItSelec->getId() << "]---" << std::endl;
        std::cout << objGraficoListaItSelec->getMatriz() << std::endl;
    }
}

void depuraListaObjGrafico(void) {
    std::cout << "--- depuraListaObjGrafico ---" << std::endl;
    for (objGraficoListaIt = objGraficoLista.begin(); objGraficoListaIt != objGraficoLista.end(); objGraficoListaIt++) {
        std::cout << "tipo: " << objGraficoListaIt->getTipo() << std::endl;
        std::cout << "bBox: " << objGraficoListaIt->getBBox()->visible << std::endl;
        ListaPoint4D ListaVerticesTMP = objGraficoListaIt->getListaVertices();
        for (ListaPoint4DIt itPto = ListaVerticesTMP.begin(); itPto != ListaVerticesTMP.end(); itPto++) {
            std::cout << "  x: " << itPto->GetX() << "  y: " << itPto->GetY() << "  z: " << itPto->GetZ() << std::endl;
        }
        std::cout << "..." << std::endl;
    }
    std::cout << "----------" << std::endl;
}

void exibeBBox(void) {
    for (objGraficoListaIt = objGraficoLista.begin(); objGraficoListaIt != objGraficoLista.end(); objGraficoListaIt++) {
        objGraficoListaIt->getBBox()->ToggleVisibility();
    }
}

#pragma mark FERRAMENTAS
//TODO: conferir calculo, "fpsDuracao >= 0.01f" era "fpsDuracao >= 1.0f"
void calculaFPS(void) {    
    // get the current tick value
    fpsCurrenteTick = VART::Time::NOW();
    // calculate the amount of elapsed seconds
    fpsTempoDecorrido = (fpsCurrenteTick - fpsUltimoTick).AsFloat();
    
    // adjust fps counter
    fpsDuracao += fpsTempoDecorrido;
    fpsFrames++;
    //printf("%d %f \n",fpsFrames,fpsDuracao);
    if (fpsDuracao >= 0.01f) {
        fps = ((double)fpsFrames / fpsDuracao);
        fpsDuracao = 0.0f;
        fpsFrames = 0;
    }
}

//TODO: ver aonde tem ponteiro e liberar
void limpaMemoria(void) {
//    ListaPontosIt = ListaPontos.begin();
//    while (ListaPontosIt != ListaPontos.end())
//    {
//        ponto = *ListaPontosIt;
//        if (ponto != NULL) {
//            delete ponto;
//            ponto = NULL;
//        }
//        ListaPontosIt++;
//    }
//    ListaPontos.clear();
    
}

#pragma mark ANIMACOES
//TODO: erro se ligar BBox e selecionar obj2 e animar ... sera mesmo um erro?
void animacao(void) {
    if (animacaoFlag) {
        if (objGraficoListaItSelec != objGraficoLista.end()) {
            transRotacao();
        }
        glutPostRedisplay();    
    }
}

#pragma mark PRINCIPAL
int main (int argc, const char * argv[]) {
    glutInit(&argc, (char **)argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition (300, 250);
	glutInitWindowSize (janelaLargura, janelaAltura);
	gJanelaPrincipal = glutCreateWindow("N4_SelecionaAlpha");
    inicializa();
    
    glutReshapeFunc(redimensiona);    
    glutDisplayFunc(desenha);
	glutKeyboardFunc(teclaPressionada);
    glutSpecialFunc (teclaPressionadaEspecial);
    glutIdleFunc(animacao);
	glutPassiveMotionFunc(mouseMovimento);
	glutMouseFunc(mouseEvento);
    //glutMotionFunc
    //glutVisibilityFunc
    //glutEntryFunc
    //glutSpaceballMotionFunc(spaceballmotion);
    //glutSpaceballRotateFunc(spaceballrotate);
    glutMainLoop();
	
    limpaMemoria();
    return 0;
}
