/// \file main.cpp
/// \brief Implementation file for "CG-N2_Point4D".
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 03/05/11.

/// Obs.: o uso de variaveis globais foram usadas por questoes didaticas mas nao sao recomendas para aplicacoes reais.

#if defined(__APPLE__) || defined(MACOSX)
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#endif
#ifdef WIN32
    #include <windows.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include <math.h>
#include "constantes.h"
#include "../../../vart/point4d.h"
#include "../../../vart/boundingbox.h"

GLint gJanelaPrincipal = 0;
GLint janelaLargura  = 400, janelaAltura = 400;
GLfloat ortho2D_minX = -400.0f, ortho2D_maxX =  400.0f, ortho2D_minY = -400.0f, ortho2D_maxY =  400.0f;
GLfloat zoomAtual = 0.0f, zoomInc = 10.0f;

VART::Point4D pto1(100,100,0);
VART::Point4D pto2(200,200,0);

void inicializacao (void) {
	glClearColor(1.0f,1.0f,1.0f,1.0);
}

void exibicaoPrincipal(void) {
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluOrtho2D(ortho2D_minX-zoomAtual, ortho2D_maxX+zoomAtual, ortho2D_minY-zoomAtual, ortho2D_maxY+zoomAtual);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	SRU();
    
    // seu desenho ...
    glColor3f(0.0, 0.0, 0.0);
    glLineWidth(3.0);
    glBegin(GL_LINES);
        glVertex2d(pto1.GetX(),pto1.GetY());
        glVertex2d(pto2.GetX(),pto2.GetY());
    glEnd();
    	
	glutSwapBuffers();
}

void teclaPressionada(unsigned char tecla, int x, int y) {
	switch (tecla) {
		case 'I': // zoom in
            zoomAtual -= zoomInc;
            glutPostRedisplay();
			break;
		case 'O': // zoom out
            zoomAtual += zoomInc;
            glutPostRedisplay();
			break;
		case 'E': // pan esquerda
            ortho2D_minX += zoomInc;
            ortho2D_maxX += zoomInc;
            glutPostRedisplay();
			break;
		case 'D': // pan direita
            ortho2D_minX -= zoomInc;
            ortho2D_maxX -= zoomInc;
            glutPostRedisplay();
			break;
		case 'C': // pan cima
            ortho2D_minY -= zoomInc;
            ortho2D_maxY -= zoomInc;
            glutPostRedisplay();
			break;
		case 'B': // pan baixo
            ortho2D_minY += zoomInc;
            ortho2D_maxY += zoomInc;
            glutPostRedisplay();
			break;
		case 'Q': // teclas esquerda
            pto1.SetX(pto1.GetX()-1);
            glutPostRedisplay();
			break;
		case 'W': // teclas direita
            pto1.SetX(pto1.GetX()+1);
            glutPostRedisplay();
			break;
    }
}

int main (int argc, const char * argv[]) {
    glutInit(&argc, (char **)argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition (300, 250);
	glutInitWindowSize (janelaLargura, janelaAltura);
	gJanelaPrincipal = glutCreateWindow("CG-N2_Point4D");
    inicializacao();
    glutKeyboardFunc(teclaPressionada);
    glutDisplayFunc (exibicaoPrincipal);
    glutMainLoop();
	
    return 0;
}
