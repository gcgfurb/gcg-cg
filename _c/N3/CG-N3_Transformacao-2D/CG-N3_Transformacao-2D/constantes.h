/// \file constantes.h
/// \brief Implementation file for "CG-N2_Mouse".
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 05/09/13.


#ifndef CG_N2_Mouse_constantes_h
#define CG_N2_Mouse_constantes_h

#if defined(__APPLE__) || defined(MACOSX)
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#endif
#ifdef WIN32
    #include <windows.h>
    #include <GL/gl.h>
    #include <GL/glut.h>
#endif
#include <list>
#include "../../../vart/point4d.h"


typedef std::list<VART::Point4D> ListaPoint4D;
typedef std::list<VART::Point4D>::iterator ListaPoint4DIt;

inline void SRU(void) {                            // Sistema de Referencia do Universo
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisable(GL_LIGHTING); //TODO: [D] FixMe: check if lighting and texture is enabled

	// eixo x
	glColor3f(1.0f, 0.0f, 0.0f);
	glLineWidth(1.0f);
	glBegin( GL_LINES );
        glVertex2f( -200.0f, 0.0f );
        glVertex2f(  200.0f, 0.0f );
	glEnd();
    
	// eixo y
	glColor3f(0.0f, 1.0f, 0.0f);
	glBegin( GL_LINES);
        glVertex2f(  0.0f, -200.0f);
        glVertex2f(  0.0f, 200.0f );
  	glEnd();
}

#endif
