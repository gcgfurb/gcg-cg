/// \file objGrafico.cpp
/// \brief informacoes do objeto grafico
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 25/05/11.

#ifndef objGrafico_h
#define objGrafico_h

#include "constantes.h"
#include "../../../vart/boundingbox.h"
#include "../../../vart/transform.h"

//#include "main.h"

#include <list>

class objGrafico {
public:
    enum objGraficoTipo { POINTS, LINES, LINE_LOOP, LINE_STRIP, TRIANGLES, TRIANGLE_STRIP,
        TRIANGLE_FAN, QUADS, QUAD_STRIP, POLYGON, SOLID_CUBE };

    objGrafico(void);
    void setId(const unsigned _id) { id = _id; };
    unsigned const getId(void) { return id; };
    void setVertice(const VART::Point4D _ponto) { ListaVertices.push_back(_ponto); };
    ListaPoint4D const getListaVertices(void) { return ListaVertices; };
    VART::BoundingBox* getBBox(void) { return &bBox; };
    void setTipo(const objGraficoTipo _tipo) { tipo = _tipo; };
    objGraficoTipo const getTipo(void) { return tipo; };
    void setCorId(const VART::Color _cor, const unsigned char _a) { cor = _cor;  cor.SetA(_a); };
    VART::Transform getMatriz(void) { return matrizObjeto; };
    void setMatriz(VART::Transform _matriz) { matrizObjeto = _matriz; };
    void matrizIdentidade(void) { matrizObjeto.MakeIdentity(); }
    bool desenha(void);
private:
    unsigned id;
    objGraficoTipo tipo;
    ListaPoint4D ListaVertices;
    ListaPoint4DIt ListaVerticesIt;
    VART::Transform matrizObjeto;
    VART::BoundingBox bBox;
    VART::Color cor;
};

#endif //objGrafico_h
