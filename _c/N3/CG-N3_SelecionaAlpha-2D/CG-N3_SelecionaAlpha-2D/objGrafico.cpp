/// \file objGrafico.cpp
/// \brief informacoes do objeto grafico
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 25/05/11.

#include "objGrafico.h"
#include <iostream>

objGrafico::objGrafico() 
:tipo(LINE_LOOP)
{
    matrizObjeto.MakeIdentity();
}

void objGrafico::calculaBoundingBox(void) {
    VART::Point4D pto;

    switch (getTipo()) {
        case POINTS:
        case LINES:
        case LINE_STRIP:
        case LINE_LOOP:
        case TRIANGLES:
        case TRIANGLE_STRIP:
        case TRIANGLE_FAN:
        case QUADS:
        case QUAD_STRIP:
        case POLYGON:
            ListaVerticesIt = ListaVertices.begin();
            pto = matrizObjeto * (*ListaVerticesIt);
            bBox.SetBoundingBox(pto.GetX(), pto.GetY(), pto.GetZ(), pto.GetX(), pto.GetY(), pto.GetZ());
            ListaVerticesIt++;
            for ( ; ListaVerticesIt != ListaVertices.end(); ListaVerticesIt++) {
                pto = matrizObjeto * (*ListaVerticesIt);
                bBox.ConditionalUpdate(pto.GetX(), pto.GetY(), pto.GetZ());
            }

            break;
        case SOLID_CUBE:
            bBox.SetBoundingBox(-0.5, -0.5, -0.5, 0.5, 0.5, 0.5);
            break;
            
        default:
            break;
    }
    bBox.ProcessCenter();
}

bool objGrafico::desenha() {
#ifdef VART_OGL
    glPushMatrix();
        glMultMatrixd(matrizObjeto.GetData());

    float fVec[4];
    cor.Get(fVec);
    glColor4fv(fVec);

    switch (getTipo()) {
        case POINTS:
        case LINES:
        case LINE_STRIP:
        case LINE_LOOP:
        case TRIANGLES:
        case TRIANGLE_STRIP:
        case TRIANGLE_FAN:
        case QUADS:
        case QUAD_STRIP:
        case POLYGON:
            glLineWidth(1);
            glBegin(getTipo());
                for (ListaVerticesIt = ListaVertices.begin(); ListaVerticesIt != ListaVertices.end(); ListaVerticesIt++) {
                    glVertex3d(ListaVerticesIt->GetX(), ListaVerticesIt->GetY(), ListaVerticesIt->GetZ());
                }
            glEnd();
            break;
        case SOLID_CUBE:
            //    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, corRed);
            //    glEnable(GL_LIGHTING);
            glutSolidCube(1.0f);
            //    glDisable(GL_LIGHTING);
            break;
            
        default:
            return false;
            break;
    }
    
    glPopMatrix();
    if (bBox.visible) {
        bBox.DrawInstanceOGL();
        glPointSize(2);
        glBegin(GL_POINTS);
            glVertex2d(bBox.GetCenter().GetX(), bBox.GetCenter().GetY());
        glEnd();
    }
    return true;
#else
    return false;
#endif
}
