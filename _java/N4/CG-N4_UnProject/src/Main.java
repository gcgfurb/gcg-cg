import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

public class Main implements GLEventListener, KeyListener, MouseListener, MouseMotionListener  {
	private GL gl;
	private GLU glu;
	private GLAutoDrawable glDrawable;
	private double xEye, yEye, zEye;
	private double xCenter, yCenter, zCenter;
	private double xUp, yUp, zUp;
	
	private ObjetoGrafico[] objetos = { 
//			new ObjetoGrafico(),
			new ObjetoGrafico() };
	
	private Random gerador = new Random();
	private Animacao animacao = new Animacao("");
	private boolean manual = true;
	
	private BoundingBox limites = new BoundingBox();
	
	private int mouseX = 0, mouseY = 0;
	private boolean calUnProject = false;

	double wcoord_Z0[] = new double[4];// wx, wy, wz;// returned xyz coords
	double wcoord_Z1[] = new double[4];// wx, wy, wz;// returned xyz coords

	class Animacao extends Thread {
	    public Animacao(String str) {
	    	super(str);
	    }
	    public void run() {
	    		while (true) {
//	    			wait(espera); //TODO: [D] colocar um tempo de espera
	    			for (byte i=0; i < objetos.length; i++) {
	    				objetos[i].Animacao(gerador.nextInt(3));
	    			}

	    			glDrawable.display();  // redesenhar ...
	    		}
	    }
	}
	
	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = drawable.getGL();
		glu = new GLU();
		glDrawable.setGL(new DebugGL(gl));

		gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		
		xEye = 0.0f; 		yEye = 40.0f; 		zEye = 0.0f;
		xCenter = 0.0f;		yCenter = 0.0f;		zCenter = 0.0f;
		xUp = 0.0f;		yUp = 0.0f;		zUp = -1.0f;
		
		for (byte i=0; i < objetos.length; i++) {
			objetos[i].atribuirGL(gl);
		}   
		
//		limites.atribuirBoundingBox(-100, 0, -100, 100, 0, 100);
		limites.atribuirBoundingBox(2, 2, 2, 18, 18, 18);
		
		wcoord_Z0[0] = wcoord_Z0[1] = wcoord_Z0[2] = 0;
		wcoord_Z1[0] = wcoord_Z1[1] = wcoord_Z1[2] = 0;
	}

	public void display(GLAutoDrawable arg0) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
		glu.gluLookAt(xEye, yEye, zEye, xCenter, yCenter, zCenter, xUp, yUp, zUp);
		
		
		gl.glLineWidth(1.0f);
		gl.glPointSize(1.0f);

		desenhaSRU3D();
		for (byte i=0; i < objetos.length; i++) {
			objetos[i].desenha();
		}
		
		if (calUnProject) {
			UnProject();	
		}
		desenhaRayCasting();
		
		gl.glFlush();
	}

	//fonte: http://www.java-tips.org/other-api-tips-100035/112-jogl/1628-how-to-use-gluunproject-in-jogl.html
	public void UnProject() {
		int viewport[] = new int[4];
		double mvmatrix[] = new double[16];
		double projmatrix[] = new double[16];
		int realy = 0;// GL y coord pos

		gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GL.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GL.GL_PROJECTION_MATRIX, projmatrix, 0);
		/* note viewport[3] is height of window in pixels */
		realy = viewport[3] - (int) mouseY - 1;
		System.out.println("Coordinates at cursor are (" + mouseX + ", " + realy);
		glu.gluUnProject((double) mouseX, (double) realy, 0.0, //
				mvmatrix, 0,
				projmatrix, 0, 
				viewport, 0, 
				wcoord_Z0, 0);
		System.out.println("World coords at z=0.0 are ( " //
				+ wcoord_Z0[0] + ", " + wcoord_Z0[1] + ", " + wcoord_Z0[2]
						+ ")");
		glu.gluUnProject((double) mouseX, (double) realy, 1.0, //
				mvmatrix, 0,
				projmatrix, 0,
				viewport, 0, 
				wcoord_Z1, 0);
		System.out.println("World coords at z=1.0 are (" //
				+ wcoord_Z1[0] + ", " + wcoord_Z1[1] + ", " + wcoord_Z1[2]
						+ ")");
		
		calUnProject = false;
		System.out.println("UnProject");
	}

	public void desenhaRayCasting() {
		// desenha RayCasting
		gl.glColor3b((byte)255, (byte)0, (byte)255);
		gl.glLineWidth(6.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3d(wcoord_Z0[0], wcoord_Z0[1], wcoord_Z0[2]);
			gl.glVertex3d(wcoord_Z1[0], wcoord_Z1[1], wcoord_Z1[2]);
		gl.glEnd();
		System.out.println("desenhaRayCasting");
	}
	
	public void desenhaFrustum() {
		gl.glBegin(GL.GL_LINE_LOOP);
//			gl.glVertex3d(arg0, arg1, arg2);
		gl.glEnd();
	}
	
	public void desenhaSRU3D() {
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f( 0.0f, 0.0f, 0.0f);
			gl.glVertex3f(20.0f, 0.0f, 0.0f);
		gl.glEnd();
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f,  0.0f, 0.0f);
			gl.glVertex3f(0.0f, 20.0f, 0.0f);
		gl.glEnd();
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f, 0.0f,  0.0f);
			gl.glVertex3f(0.0f, 0.0f, 20.0f);
		gl.glEnd();
	}
	
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
			case KeyEvent.VK_ESCAPE:
				System.exit(0);
			break;
			case KeyEvent.VK_D:
				objetos[0].debug();
			break;
			case KeyEvent.VK_R:
				objetos[0].atribuirIdentidade();
			break;
			case KeyEvent.VK_SPACE:
				//TODO: ERRO ao reinicar a thread
				manual = !manual;
				if (manual) animacao.stop();
				else animacao.start();
			break;
			case KeyEvent.VK_RIGHT:
				objetos[0].ViraEsquerda();
			break;
			case KeyEvent.VK_LEFT:
				objetos[0].ViraDireita();
			break;
			case KeyEvent.VK_UP:
				objetos[0].Acelera();
			break;
			case KeyEvent.VK_DOWN:
				objetos[0].Freia();
			break;
			case KeyEvent.VK_1:
				xEye = 0.0f; 		yEye = 40.0f; 		zEye = 0.0f;
				xUp = 0.0f;		yUp = 0.0f;		zUp = -1.0f;
			break;
			case KeyEvent.VK_2:
				xEye = 30.0f; 		yEye = 20.0f; 		zEye = 20.0f;
				xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
			break;
			case KeyEvent.VK_3:
				xEye = 0.0f; 		yEye = 200.0f; 		zEye = 00.0f;
				xUp = 0.0f;		yUp = 0.0f;		zUp = -1.0f;
			break;
			case KeyEvent.VK_4:
				xEye = 0.0f; 		yEye = 0.0f; 		zEye = 40.0f;
				xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
			break;
			case KeyEvent.VK_5:
				xEye = 60.0f; 		yEye = 60.0f; 		zEye = 60.0f;
				xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
			break;
			case KeyEvent.VK_6:
				xEye = 90.0f; 		yEye = 90.0f; 		zEye = 90.0f;
				xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
			break;
		}

		glDrawable.display();
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		gl.glViewport(0, 0, width, height);

	    glu.gluPerspective(60, width/height, 0.1, 1000);				// projecao Perpectiva 1 pto fuga 3D    
	}

	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	public void keyReleased(KeyEvent arg0) {
	}

	public void keyTyped(KeyEvent arg0) {
	}
	
	public void mouseEntered(MouseEvent e) {}
	  
	public void mouseExited(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {
//	    if ((e.getModifiers() & e.BUTTON1_MASK) != 0) {
        mouseX = e.getX();
        mouseY = e.getY();
        calUnProject = true;
//    }
        System.out.println("mousePressed");
		glDrawable.display();

	}
	    
	public void mouseReleased(MouseEvent e) {}
	    
	public void mouseClicked(MouseEvent e) {}
	    
	public void mouseDragged(MouseEvent e) {}
	    
	public void mouseMoved(MouseEvent e) {}

}
