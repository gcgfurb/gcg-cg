/**
 * Objetivo: trabalhar com conceitos de iluminacao
 * https://www.opengl.org/sdk/docs/man2/xhtml/
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
//import javax.media.opengl.glu.GLUquadric;

import com.sun.opengl.util.GLUT;

public class Main implements GLEventListener, KeyListener {
	private GL gl;
	private GLU glu;
	private GLUT glut;
	private GLAutoDrawable glDrawable;
	
    private float corVermelha[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    private float corVerde[] = { 0.0f, 1.0f, 0.0f, 1.0f };
    private float corAzul[] = { 0.0f, 0.0f, 1.0f, 1.0f };
    private float corBranca[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    private float corPreto[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    private float corAmarela[]  = {1.0f,1.0f,0.0f,1.0f};
    private float corAmarelaClara[]  = {.5f,.5f,0.0f,1.0f};
    private float cor[] = corBranca;

    private boolean temLuz = true;
    private boolean colorMaterial = false;
    
	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = drawable.getGL();
		glu = new GLU();
		glut = new GLUT();
		glDrawable.setGL(new DebugGL(gl));

		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
	    float posLight[] = { 5.0f, 5.0f, 10.0f, 0.0f };
	    gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, posLight, 0);
	    gl.glEnable(GL.GL_LIGHT0);
	    
    	gl.glColorMaterial(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE);		// depende do GL_COLOR_MATERIAL

	    gl.glEnable(GL.GL_CULL_FACE);
	    gl.glEnable(GL.GL_DEPTH_TEST);

	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		gl.glViewport(0, 0, width, height);
	    glu.gluPerspective(60, width/height, 0.1, 100);				// projecao Perpectiva 1 pto fuga 3D    
//		Debug();
	}

	public void display(GLAutoDrawable drawable) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
		glu.gluLookAt(5, 5, 5, 0, 0, 0, 0, 1, 0);

		drawAxis();
		drawCube(2.0f,2.0f,2.0f);
		gl.glPushMatrix();
		gl.glTranslated(0.0f, 0.0f, 1.5f);
			drawCube(1.0f,1.0f,1.0f);
		gl.glPopMatrix();
		
		gl.glFlush();
	}
	
	private void drawCube(float xS, float yS, float zS) {
	    if (colorMaterial) {
	    	gl.glEnable(GL.GL_COLOR_MATERIAL); // https://www.opengl.org/sdk/docs/man2/xhtml/glColorMaterial.xml
			gl.glColor3f(cor[0],cor[1],cor[2]);
	    }
	    else {
		    gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, cor, 0); // https://www.opengl.org/sdk/docs/man2/xhtml/glMaterial.xml  	
	    }
		
	    if (temLuz)
	    	gl.glEnable(GL.GL_LIGHTING);

		gl.glPushMatrix();
			gl.glScalef(xS,yS,zS);
			glut.glutSolidCube(1.0f);
		gl.glPopMatrix();
		
		gl.glDisable(GL.GL_LIGHTING);
	}

	public void drawAxis() {
		// eixo X - Red
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(10.0f, 0.0f, 0.0f);
		gl.glEnd();
		// eixo Y - Green
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 10.0f, 0.0f);
		gl.glEnd();
		// eixo Z - Blue
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 10.0f);
		gl.glEnd();
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		case KeyEvent.VK_ESCAPE:
			System.exit(1);
		break;
		case KeyEvent.VK_1:
			temLuz = !temLuz;
		break;
		case KeyEvent.VK_2:
			colorMaterial = !colorMaterial;
		break;
		case KeyEvent.VK_V:
			cor = corVermelha;
		break;
		case KeyEvent.VK_E:
			cor = corVerde;
		break;
		case KeyEvent.VK_A:
			cor = corAzul;
		break;
		case KeyEvent.VK_B:
			cor = corBranca;
		break;
		case KeyEvent.VK_P:
			cor = corPreto;
		break;
		case KeyEvent.VK_M:
			cor = corAmarela;
		break;
		case KeyEvent.VK_C:
			cor = corAmarelaClara;
		break;
		}
		glDrawable.display();	
	}
	
	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void Debug() {
	}

}
