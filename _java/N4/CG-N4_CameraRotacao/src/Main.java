import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import com.sun.opengl.util.GLUT;

public class Main implements GLEventListener, KeyListener {
	private GL gl;
	private GLU glu;
	private GLUT glut;
	private GLAutoDrawable glDrawable;
	private double xEye, yEye, zEye;
	private double xCenter, yCenter, zCenter;
	private double xUp, yUp, zUp;
	private char tipoCamera = 1;
	
	private ObjetoGrafico objeto = new ObjetoGrafico();
	
	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = drawable.getGL();
		glu = new GLU();
		glut = new GLUT();
		glDrawable.setGL(new DebugGL(gl));

		gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

		xEye = 30.0f; 		yEye = 30.0f; 		zEye = 30.0f;
		xCenter = 0.0f;		yCenter = 0.0f;		zCenter = 0.0f;
		xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
		
		objeto.atribuirGL(gl,glut);
		
        new animacao("").start();
	}

	class animacao extends Thread {
	    public animacao(String str) {
	    	super(str);
	    }
	    public void run() {
	    		while (true) {
//	    			wait(espera); //TODO: [D] colocar um tempo de espera
	    			objeto.Animacao();
	    			if (tipoCamera == '3') {
		    			xEye = objeto.obterCamera().obterX();
		    			yEye = objeto.obterCamera().obterY();
		    			zEye = objeto.obterCamera().obterZ();
	    			}
	    			glDrawable.display();  // redesenhar ...
	    		}
	    }
	}

	public void display(GLAutoDrawable arg0) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
		glu.gluLookAt(xEye, yEye, zEye, xCenter, yCenter, zCenter, xUp, yUp, zUp);
		
		gl.glLineWidth(1.0f);
		gl.glPointSize(1.0f);

		desenhaSRU3D();
		objeto.desenha();

		gl.glFlush();
	}

	public void desenhaSRU3D() {
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f( 0.0f, 0.0f, 0.0f);
			gl.glVertex3f(20.0f, 0.0f, 0.0f);
		gl.glEnd();
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f,  0.0f, 0.0f);
			gl.glVertex3f(0.0f, 20.0f, 0.0f);
		gl.glEnd();
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f, 0.0f,  0.0f);
			gl.glVertex3f(0.0f, 0.0f, 20.0f);
		gl.glEnd();
	}
	
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
		case KeyEvent.VK_R:
			objeto.atribuirIdentidade();
			break;
			case KeyEvent.VK_1:
				xEye = 0.0f; 		yEye = 40.0f; 		zEye = 0.0f;
				xUp = 0.0f;		yUp = 0.0f;		zUp = -1.0f;
				tipoCamera = '1';
			break;
			case KeyEvent.VK_2:
				xEye = 30.0f; 		yEye = 30.0f; 		zEye = 30.0f;
				xUp = 0.0f;		yUp = 1.0f;		zUp = 0.0f;
				tipoCamera = '2';
			break;
			case KeyEvent.VK_3:
				tipoCamera = '3';
			break;
		}

		glDrawable.display();
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		gl.glViewport(0, 0, width, height);
	    glu.gluPerspective(60, width/height, 0.1, 100);				// projecao Perpectiva 1 pto fuga 3D    
	}

	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	public void keyReleased(KeyEvent arg0) {
	}

	public void keyTyped(KeyEvent arg0) {
	}
}
