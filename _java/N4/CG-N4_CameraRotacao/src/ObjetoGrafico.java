import javax.media.opengl.GL;

import com.sun.opengl.util.GLUT;
public final class ObjetoGrafico {
	GL gl;
	GLUT glut;

	private Ponto4D camera = new Ponto4D();

	private Transformacao4D matrizObjeto = new Transformacao4D();
	
	public ObjetoGrafico() {
		Transformacao4D matrizTranslate = new Transformacao4D();
		matrizTranslate.atribuirTranslacao(0,0,20);
		matrizObjeto = matrizObjeto.transformMatrix(matrizTranslate);
	}

	public void atribuirGL(GL gl, GLUT glut) {
		this.gl = gl; this.glut = glut;
	}

	public void desenha() {
		gl.glColor3f(1.0f, 1.0f, 0.0f);
		gl.glLineWidth(2.0f);
		gl.glPointSize(2.0f);

		gl.glPushMatrix();
			gl.glMultMatrixd(matrizObjeto.GetDate(), 0);
			gl.glPushMatrix();
				gl.glScalef(1,1,5);
				glut.glutSolidCube(1.0f);
			gl.glPopMatrix();
			gl.glTranslated(0,0,3);
			gl.glColor3f(0.0f, 1.0f, 1.0f);
			glut.glutSolidSphere(1, 30, 30);

//			gl.glBegin(primitiva);			
//				for (byte i=0; i < vertices.length; i++) {
//					gl.glVertex3d(vertices[i].obterX(), vertices[i].obterY(), vertices[i].obterZ());
//				}
//			gl.glEnd();
		gl.glPopMatrix();
	}
	
	public void Animacao() {
		giraY(10);
	}
	
	public void giraY(double angulo) {
		Transformacao4D matrizRotacaoY = new Transformacao4D();		
		matrizRotacaoY.atribuirRotacaoY(Transformacao4D.DEG_TO_RAD * angulo);
		matrizObjeto = matrizRotacaoY.transformMatrix(matrizObjeto);

		Transformacao4D matrizCamera = new Transformacao4D();
		matrizCamera.atribuirTranslacao(0,0,20);
		matrizCamera = matrizCamera.transformMatrix(matrizObjeto);
		camera = matrizCamera.transformPoint(camera);
}
	
	public void atribuirIdentidade() {
		matrizObjeto.atribuirIdentidade();
	}

	public Ponto4D obterCamera() {
		return camera;
	}
}