import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import com.sun.opengl.util.GLUT;

public class Main implements GLEventListener, KeyListener {
	private GL gl;
	private GLU glu;
	private GLUT glut;
	private GLAutoDrawable glDrawable;
	int g_width, g_height;
	String [] fonts = { 	"BitMap 9 by 15", 
						"BitMap 8 by 13",
						"Times Roman 10 Point ", 	
						"Times Roman 24 Point ",
						"Helvetica 10 Point ",
						"Helvetica 12 Point ",
						"Helvetica 18 Point "};
	
	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = drawable.getGL();
		glu = new GLU();
		glut = new GLUT();
		glDrawable.setGL(new DebugGL(gl));
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	    gl.glEnable(GL.GL_CULL_FACE);		
	    gl.glEnable(GL.GL_DEPTH_TEST);
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		g_width = width;
		g_height = height;
	}

	private void cena3D() {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		gl.glViewport(0, 0, g_width, g_height);
	    glu.gluPerspective(60, g_width/g_height, 0.1, 1000);    

		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
		glu.gluLookAt(20.0f, 20.0f, 20.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

	    drawAxis();
	
	    gl.glColor3f(1.0f, 0.0f, 1.0f);
		glut.glutSolidCube(3.0f);		
	}

	public void cena2D() {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		glu.gluOrtho2D(0.0f, 400.0f, 400.0f, 0.0f);

		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.glLoadIdentity();
		
		gl.glColor3f(1.0f, 1.0f, 1.0f);
		gl.glBegin (GL.GL_LINE_LOOP );
		 	gl.glVertex2d(15.0f,15.0f);
		    gl.glVertex2d(260.0f,15.0f);
		    gl.glVertex2d(260.0f,160.0f);
		    gl.glVertex2d(15.0f,160.0f);
		gl.glEnd();
		
		gl.glColor3f(1.0f, 1.0f, 0.0f);
		int x = 20, y=30;
		for (int i=0; i<7;i++) {		           
			gl.glRasterPos2i(x,y);
			glut.glutBitmapString(i+2, fonts[i]);
		      y+= 20;
		}
	}
	
	public void display(GLAutoDrawable drawable) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		cena3D();		
		cena2D();
		
		gl.glFlush();
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		case KeyEvent.VK_ESCAPE:
			System.exit(1);
		break;
		}
		glDrawable.display();	
	}

	public void drawAxis() {
		// eixo X - Red
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
			gl.glVertex3f(10.0f, 0.0f, 0.0f);
		gl.glEnd();
		// eixo Y - Green
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
			gl.glVertex3f(0.0f, 10.0f, 0.0f);
		gl.glEnd();
		// eixo Z - Blue
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL.GL_LINES);
			gl.glVertex3f(0.0f, 0.0f, 0.0f);
			gl.glVertex3f(0.0f, 0.0f, 10.0f);
		gl.glEnd();
	}

	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	public void keyReleased(KeyEvent arg0) {
	}

	public void keyTyped(KeyEvent arg0) {
	}

	public void Debug() {
	}

}
