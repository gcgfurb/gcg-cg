/**
 * Objetivo: trabalhar com os tipos de modelagem de objetos
 * https://www.opengl.org/sdk/docs/man2/xhtml/
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.media.opengl.DebugGL;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
//import javax.media.opengl.glu.GLUquadric;

import com.sun.opengl.util.GLUT;

public class Main implements GLEventListener, KeyListener {
	private GL gl;
	private GLU glu;
	private GLUT glut;
	private GLAutoDrawable glDrawable;
	
    private float corRed[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    private boolean desenhoTipo = true;

	public void init(GLAutoDrawable drawable) {
		glDrawable = drawable;
		gl = drawable.getGL();
		glu = new GLU();
		glut = new GLUT();
		glDrawable.setGL(new DebugGL(gl));

		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
	    float posLight[] = { 5.0f, 5.0f, 10.0f, 0.0f };
	    gl.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, posLight, 0);
	    gl.glEnable(GL.GL_LIGHT0);

	    gl.glEnable(GL.GL_CULL_FACE);
	    gl.glEnable(GL.GL_DEPTH_TEST);

	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
	    gl.glMatrixMode(GL.GL_PROJECTION);
	    gl.glLoadIdentity();
		gl.glViewport(0, 0, width, height);
	    glu.gluPerspective(60, width/height, 0.1, 100);				// projecao Perpectiva 1 pto fuga 3D    
//		Debug();
	}

	public void display(GLAutoDrawable drawable) {
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
		glu.gluLookAt(5, 5, 5, 0, 0, 0, 0, 1, 0);

		drawAxis();

		if (desenhoTipo)
			desenhaCubo(2.0f,2.0f,2.0f);
		else
			desenhaCuboFaces();
		
		gl.glFlush();
	}
	
	private void desenhaCubo(float xS, float yS, float zS) {
	    gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, corRed, 0);
	    gl.glEnable(GL.GL_LIGHTING);

		gl.glPushMatrix();
			gl.glScalef(xS,yS,zS);
			glut.glutSolidCube(1.0f);
		gl.glPopMatrix();
		
		gl.glDisable(GL.GL_LIGHTING);
	}

	void desenhaCuboFaces()
	{
	    gl.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT_AND_DIFFUSE, corRed, 0);  // iluminacao
		gl.glEnable(GL.GL_LIGHTING);

		gl.glBegin (GL.GL_QUADS );
			// Front Face
		gl.glNormal3f(0,0,1);
			//glColor3f(1,0,0);
		gl.glVertex3f(-1.0f, -1.0f,  1.0f);
		gl.glVertex3f( 1.0f, -1.0f,  1.0f);
		gl.glVertex3f( 1.0f,  1.0f,  1.0f);
		gl.glVertex3f(-1.0f,  1.0f,  1.0f);
			// Back Face
		gl.glNormal3f(0,0,-1);
			//glColor3f(0,1,0);
		gl.glVertex3f(-1.0f, -1.0f, -1.0f);
		gl.glVertex3f(-1.0f,  1.0f, -1.0f);
		gl.glVertex3f( 1.0f,  1.0f, -1.0f);
		gl.glVertex3f( 1.0f, -1.0f, -1.0f);
			// Top Face
		gl.glNormal3f(0,1,0);
		gl.glVertex3f(-1.0f,  1.0f, -1.0f);
		gl.glVertex3f(-1.0f,  1.0f,  1.0f);
		gl.glVertex3f( 1.0f,  1.0f,  1.0f);
		gl.glVertex3f( 1.0f,  1.0f, -1.0f);
			// Bottom Face
		gl.glNormal3f(0,-1,0);
		gl.glVertex3f(-1.0f, -1.0f, -1.0f);
		gl.glVertex3f( 1.0f, -1.0f, -1.0f);
		gl.glVertex3f( 1.0f, -1.0f,  1.0f);
		gl.glVertex3f(-1.0f, -1.0f,  1.0f);
			// Right face
		gl.glNormal3f(1,0,0);
		gl.glVertex3f( 1.0f, -1.0f, -1.0f);
		gl.glVertex3f( 1.0f,  1.0f, -1.0f);
		gl.glVertex3f( 1.0f,  1.0f,  1.0f);
		gl.glVertex3f( 1.0f, -1.0f,  1.0f);
			// Left Face
		gl.glNormal3f(-1,0,0);
		gl.glVertex3f(-1.0f, -1.0f, -1.0f);
		gl.glVertex3f(-1.0f, -1.0f,  1.0f);
		gl.glVertex3f(-1.0f,  1.0f,  1.0f);
		gl.glVertex3f(-1.0f,  1.0f, -1.0f);
		gl.glEnd();
		gl.glDisable(GL.GL_LIGHTING);
	}

	
	public void drawAxis() {
		// eixo X - Red
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(10.0f, 0.0f, 0.0f);
		gl.glEnd();
		// eixo Y - Green
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 10.0f, 0.0f);
		gl.glEnd();
		// eixo Z - Blue
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glBegin(GL.GL_LINES);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 10.0f);
		gl.glEnd();
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		case KeyEvent.VK_ESCAPE:
			System.exit(1);
		break;
		case KeyEvent.VK_1:
			desenhoTipo = !desenhoTipo;
		}
		glDrawable.display();	
	}

	public void displayChanged(GLAutoDrawable arg0, boolean arg1, boolean arg2) {
	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void Debug() {
	}

}
