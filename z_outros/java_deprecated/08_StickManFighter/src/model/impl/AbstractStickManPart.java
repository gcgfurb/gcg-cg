package model.impl;

import javax.media.opengl.GL;

import model.Color;
import model.GraphicalObject;

/**
 * Classe abstrata para as partes do StickMan.
 * 
 * @author Thiago.Gesser
 */
public abstract class AbstractStickManPart extends GraphicalObject {
	
	public AbstractStickManPart() {
		super();
	}
	
	public AbstractStickManPart(Color color) {
		super(color);
	}
	
	@Override
	public final void updateBoundaringBox(GL gl) {
		//Chama o update do stick man.
		getParent().updateBoundaringBox(gl);
	}
}
