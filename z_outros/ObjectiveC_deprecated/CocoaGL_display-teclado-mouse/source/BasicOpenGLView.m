#import "BasicOpenGLView.h"

GLint zoom = 2;

enum EKeyCode
{
	KEY_BACKSPACE_KEY		= 8,
	KEY_TAB_KEY			    = 9,
	KEY_ENTER_KEY			= 13,
	KEY_ESCAPE_KEY			= 27,
    KEY_DELETE_KEY			= 127,
	KEY_SPECIAL_KEY_OFFSET  = 256,
	KEY_PLUS                = 43,
	KEY_MINUS               = 45,
	KEY_a                   = 97,           KEY_A                   = 65,
	KEY_b                   = 98,           KEY_B                   = 66,
	//KEY_0                   = 48,           KEY_1                   = 49,
};



#pragma mark ---- OpenGL Utils ----

@implementation BasicOpenGLView

// pixel format definition
+ (NSOpenGLPixelFormat*) basicPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,	// double buffered
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
        (NSOpenGLPixelFormatAttribute)nil
    };
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

// update the projection matrix based on camera and view info
- (void) updateProjection
{
    [[self openGLContext] makeCurrentContext];

	// set projection
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();

	gluPerspective(60.0, 1.0, 1.0, 10.0);
//	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 3.5);
}

// updates the contexts model view matrix for object and camera moves
- (void) updateModelView
{
    [[self openGLContext] makeCurrentContext];
	
	// move view
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
	gluLookAt(0.0, 0.0, zoom,	0.0, 0.0, 0.0,		0.0, 1.0, 0.0);
}

// handles resizing of GL need context update and if the window dimensions change, a window dimension update, reseting of viewport and an update of the projection matrix
- (void) resizeGL
{
	NSRect rectView = [self bounds];
	
	glViewport (0, 0, rectView.size.width, rectView.size.height);
	[self updateProjection];  // update projection matrix
}

#pragma mark ---- IB Actions ----
-(IBAction) animate: (id) sender { }
-(IBAction) info: (id) sender { }

#pragma mark ---- Method Overrides ----
-(void)keyDown:(NSEvent *)theEvent
{
    NSString *characters = [theEvent characters];
    if ([characters length]) {
        unichar character = [characters characterAtIndex:0];
		switch (character) {
			case 'h':
				NSLog(@"keyDown...");
				break;
			case KEY_PLUS:
				NSLog(@"Zoom + ...");
				zoom += 0.5;
				break;
			case KEY_MINUS:
				NSLog(@"Zoom - ...");
				zoom -= 0.5;
				break;
		}
		[self updateProjection]; // update projection matrix
		[self setNeedsDisplay: YES];
	}
}

- (void)mouseDown:(NSEvent *)theEvent // trackball
{
	NSLog(@"mouseDown...");
}

- (void) drawRect:(NSRect)rect
{		
	// setup viewport and prespective
	[self resizeGL]; // forces projection matrix update (does test for size changes)
	[self updateModelView];  // update model view matrix for object

	// clear our drawable
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// model view and projection matricies already set

	[self drawSRU];
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidTeapot(0.5);

	if ([self inLiveResize])
		glFlush ();
	else
		[[self openGLContext] flushBuffer];
}

// set initial OpenGL state (current context is set)
// called after context is created
- (void) prepareOpenGL
{
    long swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // set to vbl sync

	// init GL stuff here
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);    
//	glEnable(GL_CULL_FACE);
//	glFrontFace(GL_CCW);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

// this can be a troublesome call to do anything heavyweight, as it is called on window moves, resizes, and display config changes.  So be careful of doing too much here.
- (void) update // window resizes, moves and display changes (resize, depth and display config change)
{
	[super update];
}

-(id) initWithFrame: (NSRect) frameRect
{
	NSOpenGLPixelFormat * pf = [BasicOpenGLView basicPixelFormat];
	self = [super initWithFrame: frameRect pixelFormat: pf];
    return self;
}

- (BOOL)acceptsFirstResponder	{ return YES; }
- (BOOL)becomeFirstResponder	{ return YES; }
- (BOOL)resignFirstResponder	{ return YES; }

#pragma mark ---- Method News ----
- (void) drawSRU 
{
	glLineWidth(1.0);
	glColor3f(1.0f, 0.0f, 0.0f);  // axis X
	glBegin( GL_LINES );
	glVertex3f( -1.0f, 0.0f, 0.0f );
	glVertex3f(  1.0f, 0.0f, 0.0f );
	glEnd();
	
	glColor3f(0.0f, 1.0f, 0.0f);	// axis Y
	glBegin( GL_LINES);
	glVertex3f(  0.0f, -1.0f, 0.0f);
	glVertex3f(  0.0f,  1.0f, 0.0f );
  	glEnd();
	
	glColor3f(0.0f, 0.0f, 1.0f);	// axis Z
	glBegin( GL_LINES);
	glVertex3f(  0.0f, 0.0f, -1.0f);
	glVertex3f(  0.0f, 0.0f,  1.0f );
  	glEnd();
}

@end
