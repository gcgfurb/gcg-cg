#import "BasicOpenGLView.h"

#pragma mark ---- OpenGL Utils ----

@implementation BasicOpenGLView

// pixel format definition
+ (NSOpenGLPixelFormat*) basicPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,	// double buffered
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
        (NSOpenGLPixelFormatAttribute)nil
    };
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

// update the projection matrix based on camera and view info
- (void) updateProjection
{
    [[self openGLContext] makeCurrentContext];

	// set projection
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();

//	gluOrtho2D(-30.0,30.0,-30.0,30.0);
}

// updates the contexts model view matrix for object and camera moves
- (void) updateModelView
{
    [[self openGLContext] makeCurrentContext];
	
	// move view
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
}

#pragma mark ---- IB Actions ----
-(IBAction) animate: (id) sender { }
-(IBAction) info: (id) sender { }

#pragma mark ---- Method Overrides ----

- (void) drawRect:(NSRect)rect
{		
	// setup viewport and prespective
	[self updateModelView];  // update model view matrix for object

	// clear our drawable
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// model view and projection matricies already set

	[self drawSRU];
	glColor3f(1.0f, 1.0f, 1.0f);
	glutSolidTeapot(0.5);

	if ([self inLiveResize])
		glFlush ();
	else
		[[self openGLContext] flushBuffer];
}

// set initial OpenGL state (current context is set)
// called after context is created
- (void) prepareOpenGL
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);    
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

// this can be a troublesome call to do anything heavyweight, as it is called on window moves, resizes, and display config changes.  So be careful of doing too much here.
- (void) update // window resizes, moves and display changes (resize, depth and display config change)
{
	[super update];
}

-(id) initWithFrame: (NSRect) frameRect
{
	NSOpenGLPixelFormat * pf = [BasicOpenGLView basicPixelFormat];
	self = [super initWithFrame: frameRect pixelFormat: pf];
    return self;
}

#pragma mark ---- Method News ----
- (void) drawSRU 
{
	glLineWidth(1.0);
	glColor3f(1.0f, 0.0f, 0.0f);  // axis X
	glBegin( GL_LINES );
	glVertex3f( -1.0f, 0.0f, 0.0f );
	glVertex3f(  1.0f, 0.0f, 0.0f );
	glEnd();
	
	glColor3f(0.0f, 1.0f, 0.0f);	// axis Y
	glBegin( GL_LINES);
	glVertex3f(  0.0f, -1.0f, 0.0f);
	glVertex3f(  0.0f,  1.0f, 0.0f );
  	glEnd();
	
	glColor3f(0.0f, 0.0f, 1.0f);	// axis Z
	glBegin( GL_LINES);
	glVertex3f(  0.0f, 0.0f, -1.0f);
	glVertex3f(  0.0f, 0.0f,  1.0f );
  	glEnd();
}

@end
