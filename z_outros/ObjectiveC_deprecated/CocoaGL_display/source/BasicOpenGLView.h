#import <Cocoa/Cocoa.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import <OpenGL/OpenGL.h>

@interface BasicOpenGLView : NSOpenGLView
{
}

// OpenGL Utils
+ (NSOpenGLPixelFormat*) basicPixelFormat;
- (void) updateProjection;
- (void) updateModelView;

// IB Actions
-(IBAction) animate: (id) sender;
-(IBAction) info: (id) sender;

//  Method Overrides
- (void) drawRect:(NSRect)rect;
- (void) prepareOpenGL;
- (void) update;		// moved or resized
- (id) initWithFrame: (NSRect) frameRect;

// Method News
- (void) drawSRU;

@end
