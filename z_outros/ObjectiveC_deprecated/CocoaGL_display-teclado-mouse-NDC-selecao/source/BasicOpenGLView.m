#import "BasicOpenGLView.h"

GLint zoom = 2;

enum EKeyCode
{
	KEY_BACKSPACE_KEY		= 8,
	KEY_TAB_KEY			    = 9,
	KEY_ENTER_KEY			= 13,
	KEY_ESCAPE_KEY			= 27,
    KEY_DELETE_KEY			= 127,
	KEY_SPECIAL_KEY_OFFSET  = 256,
	KEY_PLUS                = 43,
	KEY_MINUS               = 45,
	KEY_a                   = 97,           KEY_A                   = 65,
	KEY_b                   = 98,           KEY_B                   = 66,
	//KEY_0                   = 48,           KEY_1                   = 49,
};



#pragma mark ---- OpenGL Utils ----

@implementation BasicOpenGLView

// pixel format definition
+ (NSOpenGLPixelFormat*) basicPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,	// double buffered
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
        (NSOpenGLPixelFormatAttribute)nil
    };
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

// update the projection matrix based on camera and view info
- (void) updateProjection
{
    [[self openGLContext] makeCurrentContext];

	// set projection
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();

 	glOrtho(orthoMin.x, orthoMax.x, orthoMin.y, orthoMax.y, 1.0, 3.5);
//	gluPerspective(60.0, 1.0, 1.0, 10.0);
//	glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 3.5);
}

// updates the contexts model view matrix for object and camera moves
- (void) updateModelView
{
    [[self openGLContext] makeCurrentContext];
	
	// move view
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity ();
  	gluLookAt(0.0, 0.0, zoom,	0.0, 0.0, 0.0,		0.0, 1.0, 0.0);
}

// handles resizing of GL need context update and if the window dimensions change, a window dimension update, reseting of viewport and an update of the projection matrix
- (void) resizeGL
{
	NSRect rectView = [self bounds];
	// deltaDes = (f - d)
	deltaDesX = orthoMax.x - orthoMin.x;
	deltaDesY = orthoMax.y - orthoMin.y;

	// deltaOri = (c - a)
	deltaOriX = rectView.size.width;
	deltaOriY = rectView.size.height;  //TODO nao sei se e sempre 2 somado para o desconto da borda, testar...

	// aspect = (deltaDes / deltaOri)
	aspectX = deltaDesX / deltaOriX;
	aspectY = deltaDesY / deltaOriY;
	
//	glViewport (0, 0, rectView.size.width, rectView.size.height);
//	NSLog(@"mudou janela");
	[self updateProjection];  // update projection matrix
//	[self setNeedsDisplay: YES];
}

#pragma mark ---- IB Actions ----
-(IBAction) animate: (id) sender { }
-(IBAction) info: (id) sender { }

#pragma mark ---- Method Overrides ----
-(void)keyDown:(NSEvent *)theEvent
{
    NSString *characters = [theEvent characters];
    if ([characters length]) {
        unichar character = [characters characterAtIndex:0];
		switch (character) {
			case 'h':
				NSLog(@"keyDown...");
				NSLog(@"ptoBegin x: %f - y: %f",ptoBegin.x,ptoBegin.y);
				NSLog(@"ptoEnd   x: %f - y: %f",ptoEnd.x,ptoEnd.y);
				NSLog(@"flagMouseDown: %i",flagMouseDown);
				break;
			case KEY_PLUS:
				NSLog(@"Zoom + ...");
				orthoMin.x -= 1.0;	orthoMin.y -= 1.0;	orthoMax.x += 1.0;	orthoMax.y += 1.0;
				break;
			case KEY_MINUS:
				NSLog(@"Zoom - ...");
				orthoMin.x += 1.0;	orthoMin.y += 1.0;	orthoMax.x -= 1.0;	orthoMax.y -= 1.0;
				break;
		}
		[self updateProjection]; // update projection matrix
		[self setNeedsDisplay: YES];
	}
}

- (void)mouseDown:(NSEvent *)theEvent // trackball
{
	if ([theEvent modifierFlags] & NSControlKeyMask) { // mouseDown + crtl
		[self rightMouseDown:theEvent];

		ptoMouseSelection = [self getPtoScreen:theEvent];
		ptoMouseSelection = [self normalizeNDCpto:ptoMouseSelection];
		selection.origin.x   = ptoMouseSelection.x - 0.5;	selection.origin.y    = ptoMouseSelection.y - 0.5; 
		selection.size.width = ptoMouseSelection.x + 0.5;	selection.size.height = ptoMouseSelection.y + 0.5; 
		flagMouseSelection = YES;
		[self isDrawRectSelection];
	}
	else {
		flagMouseSelection = NO;
		flagPtoSelection = NO;
		
		ptoBegin = [self getPtoScreen:theEvent];
		ptoBegin = [self normalizeNDCpto:ptoBegin];
	
		ptoEnd = ptoBegin;
		flagMouseDown = YES;
	}
	
	[self updateProjection]; // update projection matrix
	[self setNeedsDisplay: YES];
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	if ([theEvent modifierFlags] & NSControlKeyMask) { // mouseDown + crtl
		[self rightMouseDown:theEvent];
		
	}
	else {
		ptoEnd = [self getPtoScreen:theEvent];
		ptoEnd = [self normalizeNDCpto:ptoEnd];
	}
	
	[self updateProjection]; // update projection matrix
	[self setNeedsDisplay: YES];
}
	
- (void) drawRect:(NSRect)rect
{		
	// setup viewport and prespective
	[self resizeGL]; // forces projection matrix update (does test for size changes)
	[self updateModelView];  // update model view matrix for object

	// clear our drawable
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// model view and projection matricies already set

	[self drawSRU];
	
	if (flagMouseDown) {
		[self drawLineMouse];
	}
	
	if (flagMouseSelection) {
		[self drawRectSelection];
	}
	
	if (flagPtoSelection) {
		[self drawPtoSelection];
	}
	
	//glColor3f(1.0f, 1.0f, 1.0f);
	//glutSolidTeapot(0.5);

	if ([self inLiveResize])
		glFlush ();
	else
		[[self openGLContext] flushBuffer];
}

// set initial OpenGL state (current context is set)
// called after context is created
- (void) prepareOpenGL
{
    long swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // set to vbl sync

	// init GL stuff here
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);    
//	glEnable(GL_CULL_FACE);
//	glFrontFace(GL_CCW);
	
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	orthoMin.x = -30; orthoMin.y = -30;	orthoMax.x = 30;	orthoMax.y = 30;
	flagMouseDown = NO;
	flagMouseSelection = NO;
	flagPtoSelection = NO;
}

// this can be a troublesome call to do anything heavyweight, as it is called on window moves, resizes, and display config changes.  So be careful of doing too much here.
- (void) update // window resizes, moves and display changes (resize, depth and display config change)
{
	[super update];
}

-(id) initWithFrame: (NSRect) frameRect
{
	NSOpenGLPixelFormat * pf = [BasicOpenGLView basicPixelFormat];
	self = [super initWithFrame: frameRect pixelFormat: pf];
    return self;
}

- (BOOL)acceptsFirstResponder	{ return YES; }
- (BOOL)becomeFirstResponder	{ return YES; }
- (BOOL)resignFirstResponder	{ return YES; }

#pragma mark ---- Method News ----

// assume os positivos para X para direita e Y para cima
- (NSPoint) getPtoScreen: (NSEvent *)theEvent
{
	return [self convertPoint:[theEvent locationInWindow] fromView:nil];	
}

- (void) drawSRU 
{
	glLineWidth(1.0);
	glColor3f(1.0f, 0.0f, 0.0f);  // axis X
	glBegin( GL_LINES );
		glVertex3f( -30.0f, 0.0f, 0.0f );
		glVertex3f(  30.0f, 0.0f, 0.0f );
	glEnd();
	
	glColor3f(0.0f, 1.0f, 0.0f);	// axis Y
	glBegin( GL_LINES);
		glVertex3f(  0.0f, -30.0f, 0.0f);
		glVertex3f(  0.0f,  30.0f, 0.0f );
  	glEnd();
	
	glColor3f(0.0f, 0.0f, 1.0f);	// axis Z
	glBegin( GL_LINES);
		glVertex3f(  0.0f, 0.0f, -30.0f);
		glVertex3f(  0.0f, 0.0f,  30.0f );
  	glEnd();
}

- (void) drawLineMouse {
	glLineWidth(1.0);
	glColor3f(1.0f, 1.0f, 0.0f);
	glBegin( GL_LINES );
		glVertex2f( ptoBegin.x, ptoBegin.y );
		glVertex2f( ptoEnd.x,   ptoEnd.y );
	glEnd();
}

- (void) drawRectSelection {
	glLineWidth(1.0);
	glColor3f(0.0f, 1.0f, 1.0f);
	glBegin( GL_LINE_STRIP ); // GL_QUAD_STRIP );
		glVertex2f( selection.origin.x, selection.origin.y);
		glVertex2f( selection.origin.x, selection.size.height);
		glVertex2f( selection.size.width, selection.size.height);
		glVertex2f( selection.size.width, selection.origin.y);
		glVertex2f( selection.origin.x, selection.origin.y);
	glEnd();

}

- (void) drawPtoSelection {
	glPointSize(5.0);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin( GL_POINTS );
		glVertex2f( ptoSelection.x, ptoSelection.y);
	glEnd();
}

- (void) isDrawRectSelection {
	// testing first point
	if ((ptoBegin.x > selection.origin.x)	&&
		(ptoBegin.x < selection.size.width) &&
		(ptoBegin.y > selection.origin.y)	&&
		(ptoBegin.y < selection.size.height)) {
			flagPtoSelection = YES;
			ptoSelection = ptoBegin;
	} else {
		if ((ptoEnd.x > selection.origin.x)	&&
			(ptoEnd.x < selection.size.width) &&
			(ptoEnd.y > selection.origin.y)	&&
			(ptoEnd.y < selection.size.height)) {
			flagPtoSelection = YES;
			ptoSelection = ptoEnd;
		} else {
			flagPtoSelection = NO;
		}

	}
}

// eX = ((bX - aX) * (aspectX)) + dX
-(GLfloat) normalizeNDCx: (GLfloat) bX {
	CGFloat eX;
	eX = ((bX - 0) * (aspectX)) + orthoMin.x;
	return eX;
}

// eY = ((bY - aY) * (aspectY)) + dY
-(GLfloat) normalizeNDCy: (GLfloat) bY {
	GLfloat eY;
	eY = ((bY - 0) * (aspectY)) + orthoMin.y;
	return eY;
}

-(NSPoint) normalizeNDCpto: (NSPoint) b {
	NSPoint e;
	e.x = [self normalizeNDCx:b.x];
	e.y = [self normalizeNDCy:b.y];
	return e;
}

-(NSRect) normalizeNDCrect: (NSRect) r {
	NSRect rNew;
	rNew.origin      = [self normalizeNDCpto:r.origin];
	rNew.size.width  = [self normalizeNDCx:r.size.width];
	rNew.size.height = [self normalizeNDCy:r.size.height];
	return rNew;
}

@end
