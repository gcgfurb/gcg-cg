#import <Cocoa/Cocoa.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import <OpenGL/OpenGL.h>

@interface BasicOpenGLView : NSOpenGLView
//@private
{
	NSPoint orthoMin;	NSPoint orthoMax;
	CGFloat deltaDesX;	CGFloat deltaOriX;		CGFloat deltaDesY;	CGFloat deltaOriY;
	GLfloat aspectX;	CGFloat aspectY;

	BOOL flagMouseDown;
	NSPoint ptoBegin;
	NSPoint ptoEnd;

	BOOL flagMouseSelection;
	NSPoint ptoMouseSelection;
	
	BOOL flagPtoSelection;
	NSPoint ptoSelection;
	NSRect selection;
}

// OpenGL Utils
+ (NSOpenGLPixelFormat*) basicPixelFormat;
- (void) updateProjection;
- (void) updateModelView;
- (void) resizeGL;

// IB Actions
-(IBAction) animate: (id) sender;
-(IBAction) info: (id) sender;

//  Method Overrides
- (void) keyDown:(NSEvent *)theEvent;
- (void) mouseDown:(NSEvent *)theEvent;
- (void)mouseDragged:(NSEvent *)theEvent;			
- (void) drawRect:(NSRect)rect;
- (void) prepareOpenGL;
- (void) update;		// moved or resized
- (id) initWithFrame: (NSRect) frameRect;
- (BOOL) acceptsFirstResponder;
- (BOOL) becomeFirstResponder;
- (BOOL) resignFirstResponder;

// Method News
- (NSPoint) getPtoScreen: (NSEvent *)theEvent;
- (void) drawSRU;
- (void) drawLineMouse;
- (void) drawRectSelection;
- (void) drawPtoSelection;
- (void) isDrawRectSelection;
-(GLfloat) normalizeNDCx: (GLfloat) bX;
-(GLfloat) normalizeNDCy: (GLfloat) bY;
-(NSPoint) normalizeNDCpto: (NSPoint) b;
-(NSRect) normalizeNDCrect: (NSRect) r;

@end
