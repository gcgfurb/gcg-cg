/// \file main.cpp
/// \brief N4_C_SelecionaAlpha: demonstra o uso do campo alpha da cor para fazer selecao de objetos.
/// \version $Revision: 1.0 $
/// \author Dalton Reis.
/// \date 03/05/11.
/// Obs.: variaveis globais foram usadas por questoes didaticas mas nao sao recomendas para aplicacoes reais.

#ifndef main_h
#define main_h

#if defined(__APPLE__) || defined(MACOSX)
    #include <OpenGL/gl.h>
    #include <GLUT/glut.h>
#else
    #ifdef WIN32
        #include <windows.h>
        #include <GL/gl.h>
        #include <GL/glut.h>
    #endif
#endif

#include "VART_C/point4d.h"

#include <list>
#include <string>

enum EKeyCode
{
    RAS_ESCAPE_KEY			= 27,
    RAS_SPECIAL_KEY_OFFSET  = 256,
    RAS_1                   = 49,
    RAS_2                   = 50,
    RAS_3                   = 51,
    RAS_a                   =  97,          RAS_A                   = 65,
    RAS_b                   =  98,          RAS_B                   = 66,
    RAS_c                   =  99,          RAS_C                   = 67,
    RAS_m                   = 109,          RAS_M                   = 77,
    RAS_o                   = 111,          RAS_O                   = 79,
    RAS_r                   = 114,          RAS_R                   = 82,
    RAS_LEFT_ARROW_KEY		= GLUT_KEY_LEFT,
    RAS_UP_ARROW_KEY		= GLUT_KEY_UP,
    RAS_RIGHT_ARROW_KEY		= GLUT_KEY_RIGHT,
    RAS_DOWN_ARROW_KEY		= GLUT_KEY_DOWN,
    RAS_PAGE_UP_KEY			= GLUT_KEY_PAGE_UP,
    RAS_PAGE_DOWN_KEY		= GLUT_KEY_PAGE_DOWN,
    RAS_HOME_KEY			= GLUT_KEY_HOME,
    RAS_END_KEY				= GLUT_KEY_END,
};

typedef std::list<VART::Point4D> ListaPoint4D;
typedef std::list<VART::Point4D>::iterator ListaPoint4DIt;


void inicializa(void);
void criaCena(void);
void transRotacao(void);
void transEscalaAmplia(void);
void transEscalaReduz(void);
void redimensiona(int w, int h);
void desenha(void);
void desenhaSRU(void);
void teclaPressionadaEspecial(int tecla, GLint px, GLint py);
void teclaPressionada(unsigned char tecla, GLint x, GLint y);
void depuraMatriz(void);
void depuraListaObjGrafico(void);
void exibeBBox(void);
void limpaMemoria(void);
void animacao(void);

void desenhaTexto(int x, int y, std::string text);
GLubyte pegaPixelTela(GLint cursorX, GLint cursorY);
void mouseMovimento(GLint x, GLint y);
void mouseEvento(GLint botao, GLint estado, GLint x, GLint y);
void calculaFPS(void);

#endif //main_h

